import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { OktaAuth } from "@okta/okta-auth-js";

export default function Home() {
  const router = useRouter();
  const [isUnAuthenticated, setIsUnAuthenticated] = useState(null);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const oktaAuth = new OktaAuth({
    issuer: "https://dev-5484644.okta.com/oauth2/default",
    clientId: "0oa432ubnHH1AmIAC5d6",
    redirectUri: "http://localhost:3000/login/callback",
    pkce: true
  });

  useEffect(() => {
    oktaAuth.authStateManager.subscribe(authState => {
      if (authState?.isAuthenticated) {
        router.push("/clients");
        return;
      }

      setIsUnAuthenticated(true);
    });

    oktaAuth.authStateManager.updateAuthState();
  });

  const handleSubmit = e => {
    e.preventDefault();

    oktaAuth
      .signInWithCredentials({ username, password })
      .then(res => {
        const sessionToken = res.sessionToken;
        oktaAuth.signInWithRedirect({ sessionToken });
      })
      .catch(err => console.log("Found an error", err));
  };

  const handleUsernameChange = e => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = e => {
    setPassword(e.target.value);
  };

  return (
    <>
      {isUnAuthenticated && (
        <div className="login-form">
          <main className="content box">
            <h1 className="title">Custom Okta Login UI</h1>
            <p className="subtitle">NextJs Framework</p>
            <div className="field">
              <p className="control">
                <label className="label">Email</label>
                <input
                  className="input"
                  type="email"
                  placeholder="Email"
                  onChange={handleUsernameChange}
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-envelope"></i>
                </span>
                <span className="icon is-small is-right">
                  <i className="fas fa-check"></i>
                </span>
              </p>
            </div>
            <div className="field">
              <p className="control">
                <label className="label">Password</label>
                <input
                  className="input"
                  type="password"
                  placeholder="Password"
                  onChange={handlePasswordChange}
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-lock"></i>
                </span>
              </p>
            </div>
            <div className="field">
              <p className="control">
                <button className="button is-success" onClick={handleSubmit}>
                  Login
                </button>
              </p>
            </div>
          </main>
        </div>
      )}

      <style jsx>{`
        .login-form {
          display: flex;
          height: 100vh;
          align-items: center;
          justify-content: center;
        }

        .content {
          min-width: 24em;
        }
      `}</style>
    </>
  );
}
