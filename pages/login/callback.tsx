import { useEffect } from "react";
import { OktaAuth } from "@okta/okta-auth-js";

export default function Clients() {
  const authClient = new OktaAuth({
    issuer: "https://dev-5484644.okta.com/oauth2/default",
    clientId: "0oa432ubnHH1AmIAC5d6",
    redirectUri: "http://localhost:3000/login/callback",
    pkce: true
  });

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        if (authClient.isLoginRedirect()) {
          await authClient.handleLoginRedirect();
        } else if (!(await authClient.isAuthenticated())) {
          // Start the browser based oidc flow, then parse tokens from the redirect callback url
          console.log("signInWithRedirect: ");
          authClient.signInWithRedirect();
        } else {
          console.log("User is authenticated: ");
          // User is authenticated
        }
      } catch (error) {
        console.log("error: ", error);
      }
    };
    checkAuthentication();
  });

  return <></>;
}
