module.exports = {
  plugins: ["prettier", "import", "react-hooks", "sonarjs"],
  root: true,
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },
  settings: {
    react: {
      version: "detect"
    }
  },
  env: {
    browser: true,
    amd: true,
    node: true
  },
  overrides: [
    {
      files: ["*.js", "*.jsx"],
      rules: {
        "@typescript-eslint/explicit-module-boundary-types": "off"
      }
    }
  ],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
    "plugin:sonarjs/recommended"
  ],
  rules: {
    "react/react-in-jsx-scope": "off",
    indent: "off",
    "import/imports-first": ["error", "absolute-first"],
    "import/newline-after-import": "error",
    "import/no-extraneous-dependencies": 0,
    "import/prefer-default-export": 0,
    "no-console": 0,
    "no-param-reassign": ["error", { props: false }],
    "max-depth": ["error", 4],
    "max-nested-callbacks": ["error", 3],
    "max-len": [
      "error",
      {
        code: 100,
        ignoreUrls: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExpLiterals: true
      }
    ],
    "prettier/prettier": "error",
    quotes: ["error", "double"],
    "react/jsx-filename-extension": [
      1,
      {
        extensions: [".ts", ".tsx", ".jsx", ".js"]
      }
    ],
    "react/jsx-props-no-spreading": 0,
    "react/no-danger": 0,
    "react/prop-types": 0,
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error"
  }
};
